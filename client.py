#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socket
import sys

USAGE = 'Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>'


def parse_args():
    valid_methods = ("INVITE", "BYE", "ACK")
    try:
        if sys.argv[1] in valid_methods:
            method = sys.argv[1]
        else:
            sys.exit(USAGE)
        msg = sys.argv[2]
        recipient, server = msg.split("@")
        ip, port = server.split(":")
        port = int(port)
    except:
        sys.exit(USAGE)

    return method, recipient, ip, port


def create_request(method, recipient, ip, port):
    if method == "INVITE":
        message = f"{method} sip:{recipient}@{ip} SIP/2.0\n"
        body = f"v=0\no={recipient}@gotham.com {ip}\ns=mysession\nt=0\nm=audio {port} RTP\n"
        body_length = len(body.encode('utf-8'))
        header = f"Content-Type: application/sdp\nContent-Length: {body_length}\n\n"
        request = message + header + body
    else:
        request = f"{method} sip:{recipient}@{ip} SIP/2.0\n\n"

    return request


def main():
    method, recipient, ip, port = parse_args()
    request = create_request(method, recipient, ip, port)

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.sendto(request.encode('utf-8'), (ip, port))

            finished = False
            while not finished:
                data = sock.recv(1024)
                ack_method = data.decode("utf-8").split()[1]
                print(data.decode('utf-8'))
                if ack_method == '200':
                    request = f"ACK sip:{recipient}@{ip} SIP/2.0"
                    sock.sendto(request.encode('utf-8'), (ip, port))
                    finished = True
                else:
                    finished = False

    except ConnectionRefusedError:
        print("Error connecting to the server")


if __name__ == "__main__":
    main()
