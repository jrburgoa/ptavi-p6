import socketserver
import sys
import simplertp
import random


class MySIPHandler(socketserver.BaseRequestHandler):

    def handle(self):
        valid_methods = ("INVITE", "BYE", "ACK")
        data = self.request[0]
        sock = self.request[1]
        received_data = data.decode('utf-8')
        print(f"{self.client_address[0]} {self.client_address[1]} {received_data}")
        method = received_data.split()[0]

        if not isinstance(received_data, str):
            response = 'SIP/2.0 400 Bad Request'
            sock.sendto(response.encode('utf-8'), self.client_address)

        if method not in valid_methods:
            response = 'SIP/2.0 405 Method Not Allowed'
            sock.sendto(response.encode('utf-8'), self.client_address)

        if method == valid_methods[0]:
            trying_response = 'SIP/2.0 100 TRY'
            sock.sendto(trying_response.encode('utf-8'), self.client_address)
            ring_response = 'SIP/2.0 180 RING'
            sock.sendto(ring_response.encode('utf-8'), self.client_address)
            body = "\n".join(received_data.split("\n")[1:])
            response = 'SIP/2.0 200 OK\n' + body
            sock.sendto(response.encode('utf-8'), self.client_address)

        if method == valid_methods[1]:
            ok_response = 'SIP/2.0 200 OK'
            sock.sendto(ok_response.encode('utf-8'), self.client_address)

        if method == valid_methods[2]:
            random_num = random.randint(0, 50000)
            ip = self.client_address[0]
            port = self.client_address[1]
            audio_file = sys.argv[3]

            rtp_header = simplertp.RtpHeader()
            rtp_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=random_num)
            audio = simplertp.RtpPayloadMp3(audio_file)
            simplertp.send_rtp_packet(rtp_header, audio, ip, port)


def main():
    usage = "Usage: python3 server.py <IP> <port> <audio_file>"
    if len(sys.argv) != 4:
        sys.exit(usage)
    try:
        ip, port, audio_file = sys.argv[1:]
        port = int(port)
    except ValueError:
        sys.exit("The port must be an integer")

    try:
        server = socketserver.UDPServer((ip, port), MySIPHandler)
        print(f"Server listening on port {port}")
    except OSError as e:
        sys.exit(f"Error while trying to listen: {e.args[1]}.")

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        print("Server stopped")
        sys.exit(0)


if __name__ == "__main__":
    main()
